# README #

## Install ##
* need java 1.8+
on ubuntu
```
sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update
sudo apt-get install oracle-java8-installer
```
* download stanford parser with caseless models
```
wget http://nlp.stanford.edu/software/stanford-corenlp-full-2015-12-09.zip
wget http://nlp.stanford.edu/software/stanford-english-corenlp-2016-01-10-models.jar
```
* extract to one dir

## Run ##
* start server
```
java -mx4g -cp "*" edu.stanford.nlp.pipeline.StanfordCoreNLPServer
```

* try parsing
```
python interpreter.py
Type sentence, press enter, then ctrl+d...
search for red nike shoes
{
    "interpreter": {
        "action": "search", 
        "object": "red nike shoes", 
        "raw-text": "search for red nike shoes"
    }
}
nike shoes
{
    "interpreter": {
        "action": "unknown", 
        "object": "", 
        "raw-text": "nike shoes"
    }
}
```