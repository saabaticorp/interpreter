import ConfigParser, os
import json
import sys
import re

from corenlp import StanfordCoreNLP

#some global params
DEBUG = 0
configFile = 'corenlp.proporties'
testFile   = 'test_sentences.v1.txt.f'

searchIntent = {
                'search', 
                'find', 
                'look',
                'show',
                'get',
                'need',
                'want',
                'see',
                'give',
                'have',
                'buy',
                }

filterIntent = {
                'filter',
                'refine',
                }


class Interpreter():
    def __init__(self, configFile="corenlp.proporties"):
        config = ConfigParser.ConfigParser()
        config.read(configFile)

        self.props = dict( config.items('props') )
        self.nlp = StanfordCoreNLP('http://localhost:9000')

    def interpret(self, text):
        text = text.strip()
        nlpOutJson = self.nlp.annotate(text, properties=self.props)
        nlpOut = json.loads ( nlpOutJson )
        rootIndex = findRootIndex( nlpOut )
        rootLemma = findLemma(nlpOut, rootIndex)
        (action, subj, obj) = interpret(nlpOut, rootIndex, rootLemma)
        res = {'interpreter':               
                {
                 'raw-text': text,
                 'action': action,
                 'object':obj,
                 # 'subject': subj,               
                }
               }
        if DEBUG:
            printWordTags(nlpOut)
        return res


def printWordTags(nlpOut):
    for t in nlpOut['sentences'][0]['tokens']:
        print "%s/%s" % (t['word'], t['pos']), 
    print ""
    
#create triplets 
"""
Extract dependency triples of the form:
((head word, head tag), rel, (dep word, dep tag))
"""
def findRootIndex(nlpOut):
    (rootWord, rootIndex) = ("", 0)
    depTriplets = []
    prev = None
    for i in nlpOut['sentences'][0]['collapsed-ccprocessed-dependencies']:
        lemma = nlpOut['sentences'][0]['tokens'][i['dependent']-1]['lemma']
        if i['dep'] == 'ROOT':
            rootWord  = i['dependentGloss']
            rootIndex = i['dependent']
        #please followed by search
        elif ( ( i['dependentGloss'] in searchIntent or lemma in searchIntent)
               and prev and prev['dep']=='ROOT' ):
            rootWord  = i['dependentGloss']
            rootIndex = i['dependent']
        #i want to see shoes
        elif ( (i['dependentGloss'] in searchIntent or lemma in searchIntent)
               and (i['dep']=='xcomp' or i['dep']=='ccomp')
               and i['governor'] == rootIndex ):
            rootWord  = i['dependentGloss']
            rootIndex = i['dependent']
        depTriplets.append( ( i['governorGloss'], i['dep'], i['dependentGloss']))
        prev = i
    if DEBUG:
        for d in depTriplets:
            print d
    return rootIndex

#look what depends on root, split to subj, obj
#for the obj, add what depends on it
def interpret(nlpOut, rootIndex, rootLemma):
    subj = []
    obj  = []
    ind = 1
    for i in nlpOut['sentences'][0]['tokens']:
        if i['index'] < rootIndex:
            subj.append( (i['word'], i['pos']) )
        elif i['index'] == rootIndex:
            pass #rootLemma = i['lemma']
        else: # i['index'] > rootIndex:
            #discard IN/DT at beginning of obj span
            if obj or (not obj and i['pos']!='IN' and i['pos']!='DT' and i['pos']!='PRP'):           
                obj.append( (i['word'], i['pos']) )
        ind += 1
    #remove trailing punc in obj
    i=len(obj)-1
    while i>0:
        if obj[i][1] == '.':
            obj.pop()
        else:
            break
        i-=1
    
    subj = ' '.join([w for w,t in subj])
    objs = ""
    for w,t in obj:
        if t == 'POS':
            objs += w
        else:
            objs += ' '+w
    objs = objs.strip() 
    #action
    if rootLemma in searchIntent:
        action="search"
    elif rootLemma in filterIntent:
        action="filter"
    else:
        action="unknown"
    
    return (action, subj, objs)

def findLemma(nlpOut, index):
    return nlpOut['sentences'][0]['tokens'][index-1]['lemma']


testIntepreter = []
if DEBUG:
    with open(testFile) as f:
        for l in f:
            l = l.strip()
            if not re.match("^#", l):
                query_result = l.split(" ||| ")
                testIntepreter.append(query_result)


def main():
    myI = Interpreter()
    global DEBUG
    if DEBUG == 1:
        DEBUG = 0
        for t in testIntepreter:
            res = myI.interpret(t[0])
            comp = res['action']+" "+res['obj']
            if comp != t[1]:
                print "ERROR: assetion failed for sentence:[%s], parse:[%s], ref:[%s]" %(t[0], comp, t[1])
        DEBUG = 1
    print "Type sentence, press enter, then ctrl+d..."        
    for text in sys.stdin:
        print json.dumps(myI.interpret(text), indent=4)




if __name__ == "__main__":
    main()
    sys.exit(0)